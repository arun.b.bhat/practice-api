package Api

import akka.actor.{Actor, ActorSystem, Props}
import com.typesafe.config.ConfigFactory

object projectFetchApi extends App {
  val system = ActorSystem("FileFetchAPI")

  /*object FileType {

    val imagefiles = Seq(".jpg", ".jpeg", ".jfif", ".webp", ".png")
    val textfiles = Seq(".txt", ".doc", ".docx", ".wpd", ".pdf")
    val audiofiles = Seq(".mp3")
    val videofiles = Seq(".avi", ".mp4")
  }*/

  class FileType extends Actor {

    // import FileType._

    override def receive: Receive = {
      case message: String => {
        if (message.endsWith(".txt") || message.endsWith(".doc") || message.endsWith(".pdf")) {
          println ("its a text file")
          val childref = context.actorOf(Props[txtChild])
          childref ! message
        }
        else if (message.endsWith(".jpg") || message.endsWith(".jpeg") || message.endsWith(".png")) {
          println(" Its a Image File")
          val childref = context.actorOf(Props[imgChild])
          childref ! message
        }
        else if  (message.endsWith(".mp3")) {
          println("its an Audio file")
          val childref = context.actorOf(Props[audioChild])
          childref ! message
        }
      }
      case _ => println("Request Unknown")
     /* case message: String => {
        if (message.endsWith(".jpg") || message.endsWith(".jpeg") || message.endsWith(".png")) {
          println(" Its a Image File")
          val childref = context.actorOf(Props[imgChild])
          childref ! message
        }
        //else println("Invalid Request2")
      }
      case message: String => {
        if (message.endsWith(".mp3")) {
          println("its an Audio file")
          val childref = context.actorOf(Props[audioChild])
          childref ! message
        }
        //else println("Invalid Request3")
      }
      case _ => println("Its an invalid request")*/
    }
  }

  class txtChild extends Actor{
    override def receive: Receive = {
      case message:String => val FileAddress =ConfigFactory.load("json/filename.json")
        println(s"[File Address]: ${FileAddress.getString(message)}")
    }
  }
  class imgChild extends Actor {
    override def receive: Receive = {
      case message:String => val FileAddress =ConfigFactory.load("json/filename.json")
      println(s"[File Address]:${FileAddress.getString(message)}")
    }
  }

  class audioChild extends Actor {
    override def receive: Receive = {
      case message:String => val FileAddress =ConfigFactory.load("json/filename.json")
        println(s"[File Address]:${FileAddress.getString(message)}")
    }
  }

  val fileTypeActor = system.actorOf(Props[FileType])
  fileTypeActor ! "text.txt"
  fileTypeActor ! "img.jpg"
  fileTypeActor ! "audio.mp3"
  fileTypeActor ! "text.pdf"
  fileTypeActor ! 33
/*
  val result = "arun.jfif"
  // val split = result.split(".").toList
  val test = (result.endsWith(".jpg") || result.endsWith(".jpeg") || result.endsWith(".jfif"))
  //println(split)
  println(result)
  println(test)

  val FileAdresses =ConfigFactory.load("json/filename.json")
  println(FileAdresses)*/
}
